#!/usr/bin/env python
# Hathi : l'elephant, le seigneur de la jungle et le Gardien des legendes, respecte par toutes les creatures. Il a le droit de vie ou de mort car c'est lui qui lance l'appel de la treve de l'eau.
#
# This script transfers features from AWS to HDFS
#
import os
import sys
import datetime
import subprocess
import pipes
import time
import shutil
import hadoop_utils
from util import util_hdfs_ls, util_send_email, util_timestamp_file_to_json, util_run_cmd, util_get_platform
import re
from pid import PidFile
import json

def _log (msg):
    dt_string = datetime.datetime.now().strftime ('%Y-%m-%d %H:%M:%S')
    filename = os.path.basename(__file__)  
    msg_string = '[%s]\t%s\t[%s]\t%s' % (util_get_platform(), dt_string, filename, msg)
   
    print (msg_string)

    with open('/home/o.koch/embeddings/_%s.log' % filename,'a') as fp:
        fp.write ('%s\n' % msg_string)
        fp.close()

    # log error messages (but ignore HDFS warnings)
    if msg.find ('ERROR') is not -1 and msg.find ('WARN retry.RetryInvocationHandler') == -1:
        with open('/home/o.koch/embeddings/_%s.log.error' % os.path.basename(__file__) ,'a') as fp:
            fp.write ('%s\n' % msg_string)
            fp.close()


def _take_lock ():
    filename = '/home/o.koch/embeddings/.hathi.lock'
    assert (not os.path.isfile (filename))
    with open (filename, 'w') as fp:
        fp.write('0')

def _is_locked ():
    filename = '/home/o.koch/embeddings/.hathi.lock'
    return os.path.isfile (filename)

def _release_lock ():
    filename = '/home/o.koch/embeddings/.hathi.lock'
    assert (os.path.isfile (filename))
    os.remove(filename)

def exists_remote(host, path):
    proc = subprocess.Popen(['ssh', host, 'test -f %s' % pipes.quote(path)])
    proc.wait()
    return proc.returncode == 0

def exists_hdfs(path):
    proc = subprocess.Popen(['hadoop','fs','-test','-d',path])
    proc.wait()
    return proc.returncode == 0


def move_aws_directory (host, src, dest):
    proc = subprocess.Popen(['ssh', host, 'mv %s %s' % (pipes.quote(src),pipes.quote(dest))])
    proc.wait()


def find_first_timestamp_remote(host, path):
    proc = subprocess.Popen(['ssh', host, 'ls -td -- %s/*/.success.akela | tail -n 1' % pipes.quote(path)],\
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    result = proc.stdout.readlines()
    if result == []:
        error = proc.stderr.readlines()
        _log ('%s' % error)
        return None
    return os.path.join (path, result[0].strip())


def list_remote_dirs (host, path):
    proc = subprocess.Popen(['ssh', host, 'ls -td -- %s/*/.success.akela' % pipes.quote(path)],\
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    result = proc.stdout.readlines()
    if result == []:
        error = proc.stderr.readlines()
        _log ('%s' % error)
        return None
    dirs = []
    for filename in result:
        dirs.append(os.path.dirname(filename.strip()))
    return dirs


def list_partners():
    # load list of partner ids to look for
    partnerlistfile = '/home/o.koch/embeddings/lastPartnerTimestamp.txt'
    with open(partnerlistfile,'r') as fh:
        d = fh.readlines()
        partners = [int(x.split(' ')[0]) for x in d]

    return partners


def read_json_file (json_file):
    """ read a json file and return pairs of external hash ids"""

    pairs = []
    with open(json_file,'r') as fp:
        for line in fp.readlines():
            try:
                input_data = json.loads(line)
            except:
                _log('*** ERROR *** Failed to read JSON file %s.  Exiting.' % json_file)
            key_external_id = input_data['external_id_hash_64']
            nsim = len(input_data['similarities'])
            for s in range(nsim):
                val_external_id = input_data['similarities'][s]['external_id_hash_64']
                pairs.append((key_external_id,val_external_id))

    return pairs

def pairs_to_csv (partnerid, pairs):
    """ write pairs into csv file"""
    csv_file = 'coevents/coevents-%d.csv' % partnerid
    with open (csv_file, 'w') as fp:
        for pair in pairs:
            fp.write('%d,%d\n' % (pair[0], pair[1]))

    return csv_file

def remove_aws_directory (host, dirname):
    proc = subprocess.Popen(['ssh', host, 'rm -rf %s' % (pipes.quote(dirname))])
    proc.wait()


def main():

    host = 'ubuntu@54.247.119.12'
   
    # read data from HDFS
    process_dir(host)

    # remove AWS directory
    _log ('Removing directory on AWS...')
    remove_aws_directory(host, '/opt/coevents')

    # send coevents to AWS
    cmd = 'scp -r coevents %s:/opt' % host
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return

    #json_file = 'coevents.json'
    #pairs = read_json_file (json_file)
    #csv_to_file (1, pairs)


def process_dir (host):

    hdfs_dir = '/user/recocomputer/bestofs/longTermCoEvt_count/'

    # list partners
    partners = list_partners()

    # find their location on HDFS
    cmd = 'hadoop fs -text %s/current_output' % hdfs_dir
    _log(cmd)
    cmd_out, cmd_err, rc = util_run_cmd (cmd)
    if rc != 0:
        _log('*** ERROR *** %s' % cmd_err)
        return
    
    dirname = cmd_out

    # read co-events for each partner
    for partner in partners:

        hdfs_path = os.path.join (hdfs_dir, dirname, '%d' % partner)
        _log (hdfs_path)

        # list files on HDFS
        files = hadoop_utils.hdfsLs (hdfs_path)

        partner_pairs = []

        for bz_file in files:
            print (bz_file.filename)

            # extract json file
            json_file = 'coevents.json'
            cmd = 'hadoop fs -text %s | java -cp /home/jp.lamyeemui/recocomputer/lib/criteo-hadoop-recocomputer.jar com.criteo.recommendation.babelfish.utils.ProtobufUtil "ProductWithSimilarities" > %s' % (bz_file.filename, json_file)
            _log(cmd)
            cmd_out, cmd_err, rc = util_run_cmd (cmd)
            if rc != 0:
                _log('*** ERROR *** %s' % cmd_err)
                return
            
            # extract pairs from json file
            partner_pairs += read_json_file (json_file)

        # write pairs to csv file
        csv_file = pairs_to_csv (partner, partner_pairs)

        _log ('parner %d.  %d pairs.' % (partner, len(partner_pairs)))


if __name__ == "__main__":
    main()

