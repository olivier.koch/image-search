import os
import glob
import argparse
import multiprocessing
import requests
import shutil
import re
import util
import time


def list_categories (inputdir, categ2key):
    """ List categorie within a directory
    Data format : ((productid,partnerid),(category,url))
    """
    categories = []
    for filename in glob.glob(os.path.join(inputdir,'part*')):
        print (filename)
        with open (filename,'r') as fp:
            lines = fp.readlines()
            for line in lines:
                d = re.match ('\(\((\d+),(\d+)\),\((\d+),(.+)\)\)',line.strip())
                category = int(d.group(3))
                category = categ2key[category]
                categories.append (category)
    return categories


def build_job_list (inputdir, n_proc, categ2key, categ_whitelist, maxn):
    """ Build a list of URLs to download
    """
    joblist = [[] for j in range(n_proc)]
    count=0
    categ_count = {}
    for filename in glob.glob(os.path.join(inputdir,'part*')):
        with open (filename,'r') as fp:
            lines = fp.readlines()
            for line in lines:
                d = re.match ('\(\((\d+),(\d+)\),\((\d+),(.+)\)\)',line.strip())
                productid = int(d.group(1))
                partnerid = int(d.group(2))
                category = int(d.group(3))
                assert (category in categ2key)
                category = categ2key[category]
                url = d.group(4)
                # filter with whitelist
                if not category in categ_whitelist:
                    continue
                # update counters
                if not category in categ_count:
                    categ_count[category] = 1
                else:
                    categ_count[category] += 1
                # filter with capping
                if categ_count[category] > maxn:
                    continue
                joblist[count%n_proc].append((partnerid,productid,category,url))
                count += 1
    print ('built %d job lists with %d total images.' % (len(joblist),count))
    # save categ count to file
    with open ('/tmp/categ_count.csv','w') as gp:
        for k,v in categ_count.iteritems():
            gp.write('%5d,%5d\n' % (k,v))
    # save job list to file
    for k in range(n_proc):
        filename = '/tmp/joblist-%d.csv' % k
        with open (filename,'w') as gp:
            for t in joblist[k]:
                gp.write('%d,%d,%d,%s\n' % (t[0], t[1], t[2], t[3]))

    return joblist


def download_image (url, filename):
    """ download an image from the web using requests
    """
    n_attempts=10
    success = False
    for k in range(n_attempts):
        try:
            response = requests.get(url, stream=True)
        except:
            time.sleep(.2)
            continue
        if response.status_code == 200:
            with open(filename, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
                success = True
        del response
        if success:
            break
    return success


def parallel_run (output_dir, job_list):
    """ download images
    """
    for j in job_list:
        partnerid = j[0]
        productid = j[1]
        category = j[2]
        url = j[3]
        local_outdir = os.path.join (output_dir, '%d/' % category)
        if not os.path.isdir (local_outdir):
            os.makedirs (local_outdir)
        out_filename = os.path.join (local_outdir, '%d_%d.jpeg'%(partnerid, productid))
        # do not download same image twice
        if os.path.isfile (out_filename):
            continue
        print ('%s --> %s...' % (url, out_filename))
        if not download_image (url, out_filename):
            print ('ERROR : failed to download image from URL %s' % url)


def execute_list (job_lists, output_dir):
    """ Execute a job list
    """
    n_proc = len(job_lists)
    jobs = []
    for proc in range(n_proc):
        process = multiprocessing.Process (target=parallel_run, args=[output_dir, job_lists[proc]])
        process.start()
        jobs.append(process)

    # wait for all
    for job in jobs:
        job.join()


def count_images (indir):
    filelist = glob.glob(indir+'/*/*.jpeg')
    return (len (filelist))


def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    parser.add_argument('--outdir', dest='output_dir', type=str, help='output directory', required=True)
    parser.add_argument('--depth', dest='depth', type=str, help='depth of the taxonomy (use leaf by default)', required=True)
    parser.add_argument('--nproc', dest='n_procs', type=int, help='number of processes to launch', required=False, default=32)
    parser.add_argument('--maxn', dest='maxn', type=int, help='max number of items per category', required=False, default=5000)
    args = parser.parse_args()

    # load google taxonomy
    tax = util.load_taxonomy ()

    # compute category mapping
    depth = args.depth
    print("Building taxonomy with depth = %s" % depth)
    (categ2key, _, key2name, _) = util.taxonomy_map_at_depth (tax, depth)

    # stats categories
    categories_dist_file = os.path.join(args.input_dir,'categories-distribution-images-%s.csv'%depth)
    if os.path.isfile (categories_dist_file):
        categ_dist = util.read_dict (categories_dist_file)
    else:
        categories = list_categories (args.input_dir, categ2key)
        categ_dist = util.dist_stats (categories)
        util.dict_to_file (categ_dist, categories_dist_file)

    # build whitelist
    whitelist = []
    for k,v in categ_dist.iteritems():
        if v >= 100:
            whitelist.append(k)

    print ('%d categories observed. whitelist has %d categories.' % (len(categ_dist),len(whitelist)))
   
    return

    # build job list
    job_lists = build_job_list (args.input_dir, args.n_procs, categ2key, whitelist, args.maxn)

    # clear output
#    print ('cleaning output...')
#    util.reset_dir (args.output_dir)

    # execute
    execute_list (job_lists, args.output_dir)

    # count images
    n_images = count_images (args.output_dir)
    print ('output dir has %d images.' % n_images)


if __name__ == "__main__":
    main()
