import util
import argparse

def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--in', dest='input_file', type=str, help='input list of categories', required=True)
    parser.add_argument('--depth', dest='depth', type=str, help='depth of the taxonomy', required=True)
    args = parser.parse_args()

    # load google taxonomy
    tax = util.load_taxonomy ()

    # compute category mapping
    depth = args.depth
    print("Building taxonomy with depth = %s" % depth)
    (categ2key, _, key2name, key2depth) = util.taxonomy_map_at_depth (tax, depth)

    # read categories
    with open (args.input_file,'r') as fp:
        categories = [int(a.strip()) for a in fp.readlines()]

    for cat in categories:
        if key2depth[categ2key[cat]]==2:
            print ('%34s : depth %2d' %  (key2name[categ2key[cat]], key2depth[categ2key[cat]]))

if __name__ == "__main__":
    main()



