# coding: utf-8
import os
import pickle
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.preprocessing import image
from keras.models import Model
from keras.layers import Dense, AveragePooling2D, Flatten
from keras.callbacks import TensorBoard, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.metrics import top_k_categorical_accuracy
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from multi_gpu import make_parallel


def top_3(x, y):
    return top_k_categorical_accuracy(x, y, k=3)


class InceptionFinetuner:
    def __init__(self, train_dir, val_dir, base_dir, n_epochs_last_layer, n_epochs_conv_layers, batch_size, n_gpus=1):
        self.train_dir = train_dir
        self.val_dir = val_dir
        self.base_dir = base_dir
        self.n_epochs_last_layer = n_epochs_last_layer
        self.n_epochs_conv_layers = n_epochs_conv_layers
        self.train_generator = None
        self.val_generator = None
        self.model = None
        self.hist_last_layer = None
        self.hist_conv_layers = None
        self.n_output_categories = 0
        self.batch_size = batch_size * n_gpus
        self.n_gpus = n_gpus
        os.makedirs(os.path.join(self.base_dir, "model"), exist_ok=True)

    def finetune(self):
        self.get_data_generator()
        self.n_output_categories = self.train_generator.nb_class
        self.get_models()
        self.train_last_layer()
        self.train_conv_layers()

    def save_history(self):
        prefix = "last_layer"
        with open(os.path.join(self.base_dir, "log_{}".format(prefix), "history"), "wb") as history_file:
            pickle.dump(self.hist_last_layer.history, history_file, pickle.HIGHEST_PROTOCOL)
        prefix = "conv_layers"
        with open(os.path.join(self.base_dir, "log_{}".format(prefix), "history"), "wb") as history_file:
            pickle.dump(self.hist_conv_layers.history, history_file, pickle.HIGHEST_PROTOCOL)

    def get_data_generator(self):
        # Create the data generator, with data augmentation.
        train_datagen = ImageDataGenerator(
            preprocessing_function=preprocess_input,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=False,
            width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.1)
        val_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

        self.train_generator = train_datagen.flow_from_directory(
            self.train_dir,
            target_size=(299, 299),
            batch_size=self.batch_size,
            class_mode='categorical')
        self.val_generator = val_datagen.flow_from_directory(
            self.val_dir,
            target_size=(299, 299),
            batch_size=self.batch_size,
            class_mode='categorical')

        assert (self.train_generator.nb_class == self.val_generator.nb_class)

    def get_models(self):
        # Create the model
        base_model = InceptionV3(weights='imagenet', include_top=False, input_shape=(299, 299, 3))

        x = base_model.output
        # Add the missing classification layer
        x = AveragePooling2D((8, 8), strides=(8, 8), name='avg_pool')(x)
        x = Flatten(name='flatten')(x)
        predictions = Dense(self.n_output_categories, activation='softmax', name='predictions')(x)

        self.model = Model(input=base_model.input, output=predictions)

        # first: train only the top layers (which were randomly initialized)
        # i.e. freeze all convolutional InceptionV3 layers
        for layer in base_model.layers:
            layer.trainable = False

    def get_callbacks(self, prefix):
        tensorboard_cb = TensorBoard(log_dir=os.path.join(self.base_dir, "log_{}".format(prefix)))
        earlystopping_accuracy = EarlyStopping(monitor='val_acc', min_delta=0.001, patience=2, mode='max')
        model_prefix = "weight_{}".format(prefix)
        best_model_checkpoint = ModelCheckpoint(os.path.join(self.base_dir, "model",
                                                             model_prefix + ".{epoch:02d}-{val_acc:.2f}.hdf5"),
                                                monitor='val_acc', mode='max')
        return [tensorboard_cb, earlystopping_accuracy, best_model_checkpoint]

    def train_last_layer(self):
        """ Train the last layer of Inceptionv3
        """
        if self.n_gpus > 1:
            parallel_model = make_parallel(self.model, self.n_gpus)
        else:
            parallel_model = self.model

        parallel_model.compile(optimizer='rmsprop',
                                    loss='categorical_crossentropy',
                                    metrics=['accuracy', top_3])
        callbacks = self.get_callbacks("last_layer")
        print("Start training last layer")
        self.hist_last_layer = parallel_model.fit_generator(
            self.train_generator,
            samples_per_epoch=self.train_generator.nb_sample,
            nb_epoch=self.n_epochs_last_layer,
            validation_data=self.val_generator,
            nb_val_samples=self.val_generator.nb_sample,
            callbacks=callbacks,
            pickle_safe=False,
            max_q_size=self.batch_size * 2,
            nb_worker=4
        )

    def train_conv_layers(self):
        # at this point, the top layers are well trained and we can start fine-tuning
        # convolutional layers from inception V3. We will freeze the bottom N layers
        # and train the remaining top layers.
        # we chose to train the top 2 inception blocks, i.e. we will freeze
        # the first 172 layers and unfreeze the rest:
        for layer in self.model.layers[:172]:
            layer.trainable = False
        for layer in self.model.layers[172:]:
            layer.trainable = True

        if self.n_gpus > 1:
            parallel_model = make_parallel(self.model, self.n_gpus)
        else:
            parallel_model = self.model
        # we need to recompile the model for these modifications to take effect
        # we use SGD with a low learning rate
        parallel_model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
                               loss='categorical_crossentropy',
                                    metrics=['accuracy', top_3])
        callbacks = self.get_callbacks("conv_layers")
        callbacks.append(ReduceLROnPlateau(
            monitor='val_acc', factor=0.1, patience=3, verbose=0, mode='max',
            epsilon=0.001, cooldown=0, min_lr=1e-6))

        # we train our model again (this time fine-tuning the top 2 inception blocks
        # alongside the top Dense layers
        print("Start finetuning 2 conv layers")
        self.hist_conv_layers = parallel_model.fit_generator(
            self.train_generator,
            samples_per_epoch=self.train_generator.nb_sample,
            nb_epoch=self.n_epochs_conv_layers,
            validation_data=self.val_generator,
            nb_val_samples=self.val_generator.nb_sample,
            callbacks=callbacks,
            pickle_safe=False,
            max_q_size=self.batch_size * 2,
            nb_worker=4
        )


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description='Finetune InceptionV3 model using Keras')
    parser.add_argument('--traindir', type=str,
                        help='Path to the directory containing training images.')
    parser.add_argument('--valdir', type=str,
                        help='Path to the directory containing training images.')
    parser.add_argument('--logdir', type=str,
                        help='Path to the directory containing logs and checkpoints')
    parser.add_argument('--nepochs_last', type=int, default=5,
                        help='Number of epochs to train last layer')
    parser.add_argument('--nepochs_conv', type=int, default=5,
                        help='Number of epochs to finetune conv layers')
    parser.add_argument('--batch', type=int, default=32,
                        help='Batch size for train and val')
    parser.add_argument('--ngpus', type=int, default=1,
                        help='Number of gpus to use')
    main_args = parser.parse_args()

    print("Job started with {}".format(main_args))
    finetuner = InceptionFinetuner(main_args.traindir, main_args.valdir, main_args.logdir,
                                   main_args.nepochs_last, main_args.nepochs_conv, main_args.batch, main_args.ngpus)
    finetuner.finetune()
    finetuner.save_history()
