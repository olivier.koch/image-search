import sys
import os
import json
import gzip
sys.path.append(os.path.abspath('../'))
import hadoop_utils as hp


def list_hdfs_files (hdfs_root_dir, target_partnerids):
    """ List HDFS files for the universal catalog for a given set of partners
    """
    targets = {}

    print ('listing %s' % hdfs_root_dir)
    output = hp.hdfsLs (hdfs_root_dir)
    dirnames = [a.filename for a in output]
    dirnames.reverse()

    for dirname in dirnames:
        print ('\tlisting %s' % dirname)
        l_output = hp.hdfsLs (dirname)
        link_filenames = [a.filename for a in l_output]
        for link_filename in link_filenames:
            # shortcut to partnerid
            early_partnerid = int(os.path.basename(link_filename).split('_')[0])
            if not early_partnerid in target_partnerids:
                continue
            print ('getting %s' % link_filename)
            local_filename = '/tmp/partner.lnk'
            hp.hdfsCpToLocal (local_filename,link_filename)
            with open (local_filename,'r') as gp:
                line = gp.readline()
                data = json.loads (line)
                partnerid = data['partner_id']
                target = data['target']
                timestamp = data['timestamp']
                assert(partnerid == early_partnerid)
                if not partnerid in target_partnerids:
                    continue
                s = (timestamp,target)
                if not partnerid in targets:
                    targets[partnerid]=[s]
                else:
                    targets[partnerid].append(s)
                target_partnerids.remove(partnerid)
                print ('found partner %d in %s. %d partners left.' % (partnerid, link_filename,len(target_partnerids)))
            if len (target_partnerids)==0:
                break
        if len (target_partnerids)==0:
            break
    for k,v in targets.iteritems():
        targets[k] = sorted (v,key=lambda a:a[0], reverse=True)
    return targets

def process_targets (targets):
    """ Sends files to AWS 
    """
    for partnerid,files in targets.iteritems():
        # take most recent data
        timestamp = files[0][0]
        output = hp.hdfsLs (files[0][1])
        gz_files = [a.filename for a in output]
        for (gz_file,gz_count) in zip(gz_files,range(len(gz_files))):
            local_gz_file = '/tmp/uc.gz'
            hp.hdfsCpToLocal (local_gz_file,gz_file)
            # send to aws
            host = "ubuntu@54.155.38.49"
            aws_dir = '/ops/uc'
            aws_filename = '%d-%05d.gz' % (partnerid, gz_count)
            cmd = 'scp %s %s:%s'% (local_gz_file, host, os.path.join(aws_dir,aws_filename))
            print (cmd)
            os.system (cmd)

def main():
    hdfs_root_dir = ' /user/wca/langoustine_universal/categorization'
    target_partnerids = [9049,5490,19306,15929,14798,29494,23463,27827,6089]
#    targets = list_hdfs_files (hdfs_root_dir, target_partnerids)
#
#    process_targets (targets)

    for partnerid in target_partnerids:
        hdfs_dir = os.path.join (hdfs_root_dir,'%d'%partnerid)
        output = hp.hdfsLs (hdfs_dir)
        dirnames = [a.filename for a in output]
        basenames = [os.path.basename(a) for a in dirnames]
        basenames.sort(reverse=True)

        for basename in basenames:
            dirname = basename
            hdfs_dir = os.path.join (hdfs_root_dir,'%d/%s' % (partnerid,dirname))
            output = hp.hdfsLs (hdfs_dir)
            filenames = [a.filename for a in output]
            for (filename,gz_count) in zip(filenames,range(len(filenames))):
                local_gz_file = '/tmp/uc.gz'
                hp.hdfsCpToLocal (local_gz_file,filename)
                # send to aws
                host = "ubuntu@54.155.38.49"
                aws_dir = '/ops/uc'
                aws_filename = '%d-%s-%05d.gz' % (partnerid, basename, gz_count)
                cmd = 'scp %s %s:%s'% (local_gz_file, host, os.path.join(aws_dir,aws_filename))
                print (cmd)
                os.system (cmd)
            

if __name__ == "__main__":
    main()
