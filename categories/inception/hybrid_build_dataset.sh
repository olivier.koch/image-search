#!/bin/bash

bazel build inception/build_image_data

bazel-bin/inception/build_image_data \
      --train_directory=/ops/lsid/large \
        --validation_directory=/ops/brand/images \
          --output_directory=/ops/tf-data/large \
            --labels_file=/home/ubuntu/image-search/categories/lsid/large_categories.txt \
              --train_shards=128 \
                --validation_shards=24 \
                  --num_threads=8

