import os
import glob
import random
import shutil
import pandas as pd
import numpy as np
import requests
import bisect
import re
import subprocess

def argtopn (vect, N):
    """ Return the indices of the N top elements of vect
    """
    return np.argpartition (vect, -N)[-N:]


def count_white_pixels (filename,tmp_filename):
    """ count number of white pixels in an image using imagemagick
        specify tmp_filename to avoid collisions when using multiple threads """
    # convert tog grayscale
    FNULL = open(os.devnull,'w')
    proc = subprocess.Popen(['convert',filename,'-threshold','95%',tmp_filename], stdout=FNULL, stderr=FNULL)
    proc.wait()
    assert (proc.returncode==0)
    # histogram
    proc = subprocess.Popen(['convert',tmp_filename,'-format','%c','histogram:info:-'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    res = proc.stdout.readlines()
    err = proc.stdout.readlines()
    assert (proc.returncode==0)
    # count pixels
    n_white_pix = 0
    n_total_pix = 0
    for line in res:
        n_pix = int(line.split(':')[0])
        n_total_pix += n_pix
        for k in range(253,256):
            if line.find('gray(%d)'%k) is not -1:
                n_white_pix += n_pix
    return (n_white_pix,1.0*n_white_pix/n_total_pix)


def parse_line (line):
    """ Parse a line from the dataset file
    """
    line = line.strip().replace(' ','')
    d = re.match('\((\d+),(\d+),\[([\d,]+)\],\[([\d,]+)\],(\d+)\)',line)
    productid = int(d.group(1))
    partnerid = int(d.group(2))
    deltas = np.fromstring(d.group(3),sep=',',dtype=np.int32)
    weights = np.fromstring(d.group(4),sep=',')
    category = int(d.group(5))
    return (productid,partnerid,deltas,weights,category)


def read_data_from_file (filename):
    """ Read CNN features and categories from a file
    """
    cnn_vectors = None
    categories = []
    partnerids = []
    productids = []
    with open (filename,'r') as fp:
        lines = fp.readlines()
        cnn_vectors = np.zeros((len(lines),4096))
        linecount=0
        for line in lines:
            (productid, partnerid, deltas, weights, category) = parse_line (line)
            cnn_vectors[linecount,:] = sparse_to_dense (weights, deltas)
            categories.append(category)
            productids.append (productid)
            partnerids.append (partnerid)
            linecount+=1
    return (cnn_vectors, np.array(categories), np.array(productids), np.array(partnerids))


def sparse_to_dense(weights, indices_delta, size=4096, alpha=1000000):
    """Convert the sparse features stored in the protobuf to dense ndarray
    """
    indices = np.cumsum(indices_delta)
    dense = np.zeros((size, ))
    dense[indices] = np.array(weights)/(1.0 * alpha)
    return dense


def split_to_chunks (vect, chunk_size):
    """ Split vect into chunks of size smaller or equal to chunk_size
    """
    n_f = len(vect)
    n_chunks = int(n_f*1.0/chunk_size)+1
    out = [ [] for i in range(n_chunks)]
    for (f,i) in zip(vect,range(n_f)):
        out[i%n_chunks].append(f)
    return out


def dict_to_file (count, filename):
    """ write dictionary to file
    """
    with open(filename,'w') as gp:
        for k,v in count.iteritems():
            gp.write('%d,%d\n'%(k,v))


def read_dict (filename):
    """ Read a dictionary from file
    """
    d = {}
    with open (filename,'r') as gp:
        for line in gp.readlines():
            v = line.strip().split(',')
            key = int(v[0])
            val = int(v[1])
            d[key]=val
    return d

                    
def list_categories (inputdir, categ2key):
    """ List categorie within a directory
    """
    all_categories = np.array([])
    count=0
    for filename in glob.glob(os.path.join(inputdir,'part*')):
        print(filename)
        with open (filename,'r') as fp:
            x = np.load(filename)
            categories = x[:,-3].astype(np.int32)
            categories = [categ2key[a] for a in categories]
            all_categories = np.append(all_categories, categories)
            count+=1
    return all_categories


def dist_stats (vect):
    uq = np.unique(vect)
    count = {}
    for v in uq:
        ind = np.nonzero(vect==v)[0]
        count[v]=ind.size
    return count


def reset_dir (dirname):
    """ Erase and re-create a directory
    """
    if os.path.isdir (dirname):
        shutil.rmtree (dirname)
    os.makedirs (dirname)


def index(a, x):
    """ Locate the leftmost value exactly equal to x
    """
    i = bisect.bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    raise ValueError


def list_dirs (dirname):
    """ List all dirs inside a directory
    """
    files = glob.glob(dirname+'/*')
    for filename in files:
        if os.path.isdir (filename):
            yield (filename)


def load_taxonomy():
    """ Load the Google taxonomy, either from the web or from a local file
    """
    taxonomy_file = 'taxonomy.csv'
    if os.path.isfile (taxonomy_file):
        taxonomy = pd.read_csv (taxonomy_file)
        return taxonomy
    else:
        URL = "https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt"
        taxonomy = requests.get(URL).text.split("\n")[1:]

        res = []
        for i in map(lambda x: x.split('-'), taxonomy):
          if len(i) > 1:
            res.append([i[0]] + map(unicode.strip, ''.join(i[1:]).split('>')))

        taxonomy = pd.DataFrame(res, columns=[
            'categoryId',
            'Level1',
            'Level2',
            'Level3',
            'Level4',
            'Level5',
            'Level6',
            'Level7'])
        taxonomy.categoryId = taxonomy.categoryId.astype(int)
        taxonomy['leaf'] = taxonomy.apply(lambda x: x[x.notnull()].values[-1], axis=1)
        taxonomy['level_max'] = taxonomy.apply(lambda x: sum(x.notnull()), axis=1) - 2

        taxonomy.to_csv (taxonomy_file, encoding='utf-8')

        return taxonomy


def taxonomy_to_strings (tax):
    """ extract list of strings from taxonomy
    """
    res = []
    n = tax.shape[0]
    for k in range(n):
        out_s = ''
        for u in range(1,8):
            level = 'Level%d'%u
            s = tax[level][k]
            if not pd.isnull(s):
                if u>1:
                    out_s += '>'
                out_s += s.replace(' ','')
        res.append(out_s)
    return res


def taxonomy_build_distance_matrix (tax):
    n = tax.shape[0]
    dmap=np.zeros((n,n))
    for k1 in range(n):
        lmax1=tax['level_max'][k1]
        for k2 in range(k1,n):
            lmax2=tax['level_max'][k2]
            overlap=0
            for l in range(1,8):
                if tax['Level%d'%l][k1]==tax['Level%d'%l][k2]:
                    overlap+=1
                else:
                    break
            dist = max([lmax1,lmax2])-overlap
            dmap[k1][k2]=dist
            dmap[k2][k1]=dist
        if k1==3:
            break
        print('%d done' % k1)
    print(dmap[3,3])
    print(dmap[3,19])
    print (n)

def taxonomy_map_at_depth (tax, level):
    """ Build maps between categories at a given depth
    level is either Level1, ..., Level7 or leaf
    """
    # fill nan values
    tax = tax.fillna(method='bfill',axis=1)
    keys = tax[level].as_matrix()
    keys = np.unique(keys)
    nel = tax.shape[0]
    categ2key = {}
    key2categ = {}
    key2name = {}
    key2depth = {}
    # fill empty cells
    for k in range(nel):
        cl = tax[level][k]
        categoryId = int(tax['categoryId'][k])
        assert (cl in keys)
        intkey = index(keys,cl)
        categ2key[categoryId]=intkey
        if not intkey in key2categ:
            key2categ[intkey]=[]
        key2categ[intkey].append (categoryId)
        key2name[intkey] = cl
        cdepth = int(tax['level_max'][k])
        key2depth[intkey] = cdepth

    return (categ2key, key2categ, key2name, key2depth)


def map_dataset_taxonomy_depth (input_dir, output_dir, level):
    """ Converts categories in a dataset to new (mapped) 
        categories at a given depth of the taxonomy
        level is either Level1, ..., Level7 or leaf
    """
    # reset output dir
    reset_dir (output_dir)
    # load taxonomy
    tax = load_taxonomy ()
    # convert taxonomy to map
    (categ2key, key2categ, _, _) = taxonomy_map_at_depth (tax, level)
    # list files
    filelist = list(glob.glob(os.path.join(input_dir,'part-*')))
    # replace categories by mapped categories
    count=0
    nfiles = len(filelist)
    for filename in filelist:
        basename = os.path.basename (filename)
        out_filename = os.path.join (output_dir, basename)
        print ('[%4d/%4d] %s --> %s' % (count, nfiles, filename, out_filename))
        op = open (out_filename,'w')
        with open (filename,'r') as fp:
            for line in fp.readlines():
                categoryId = int(line.split(',')[-1].split(')')[0])
                assert (categoryId in categ2key)
                mapped_cat = categ2key[categoryId]
                line = line.replace('%d)'%categoryId,'%d)'%mapped_cat)
                op.write (line)
        op.close ()
        count += 1
        

def map_categories ():
    for k in [1,3,5]:
        input_dir = '/opt/extractFullShuffledTrain'
        output_dir = '/opt/extractFullShuffledTrainLevel%d'%k
        map_dataset_taxonomy_depth (input_dir, output_dir, 'Level%d'%k)
        input_dir = '/opt/extractFullShuffledTest'
        output_dir = '/opt/extractFullShuffledTestLevel%d'%k
        map_dataset_taxonomy_depth (input_dir, output_dir, 'Level%d'%k)


def resample (vect):
    """ Resample a vector of ints
    """
    indices = np.array([],dtype=np.int32)
    # compute distribution
    uq = np.unique(vect)
    count = np.zeros((uq.size))
    nz = {}
    for (k,i) in zip(uq,range(uq.size)):
        nz[k] = np.nonzero(vect==k)[0]
        count[i] = nz[k].size
    med = int(np.median (count))
    # cap all instances > med
    for (k,i) in zip(uq,range(uq.size)):
        ind = nz[k]
        if count[i]> med:
            print ('downsampling item %f, appeared %d times, median = %d' % (k, ind.size, med))
            np.random.shuffle (ind)
            ind = ind[:med]
        indices = np.append(indices,ind)
    
    return (vect[indices],indices)


def check_jpeg_file (filename):
    """ check whether a JPEG file is valid, using djpeg
    """
    FNULL = open(os.devnull,'w')
    proc = subprocess.Popen(['djpeg','-fast','-grayscale','-onepass',filename], stdout=FNULL, stderr=FNULL)
    proc.wait()
    return proc.returncode == 0

def main():
    """ Main function
    """
    tax = load_taxonomy ()
    taxonomy_build_distance_matrix (tax)
    return

    filename = '/ops/lsid/images/3444/google_282.jpg'
    tmp_filename = '/tmp/.tmp.jpg'
    (n,r) = count_white_pixels (filename,tmp_filename)
    print(n,r)
    return
    u = np.array([1,2,3,3,4,4,2,1, 1, 1, 1, 1, 1])
    print (resample(u))
    pass

if __name__ == "__main__":
    main()


