import argparse
import util
import glob
import numpy as np

def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    args = parser.parse_args()

    dirs = util.list_dirs (args.input_dir)
    counts= np.array([])
    count_dict = {}
    count_dict_inverse = {}
    total_count=0
    for dirname in dirs:
        files = glob.glob(dirname+'/*.jp*g')
        nfiles = len(files)
        category = dirname.split('/')[-1]
        # add to dict
        count_dict[category]=nfiles
        # add to inverse dict
        if nfiles in count_dict_inverse:
            count_dict_inverse[nfiles].append(category)
        else:
            count_dict_inverse[nfiles] = [category]
        counts = np.append(counts,nfiles)
        total_count += nfiles

    thresh=300
    for k,v in count_dict.iteritems():
        if v<thresh:
            print ('Warning : dir %s has %d < %d images' % (k,v,thresh))
    thresh=1500
    for k,v in count_dict.iteritems():
        if v>thresh:
            print ('Warning : dir %s has %d > %d images' % (k,v,thresh))


    print (counts)
    print ('Min   # images: %d' % np.min(counts))
    print ('Max   # images: %d' % np.max(counts))
    print ('Avg   # images: %.1f' % np.mean(counts))
    print ('Total # images: %d' % total_count)


if __name__ == "__main__":
    main()

