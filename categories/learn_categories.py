import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import os
import numpy as np
import glob
import random
import time
import logging
import ConfigParser
import util
import argparse
from sklearn.preprocessing import StandardScaler
from datetime import datetime
import random

_logger = None


def mnist_example ():
    """ Run the plain-vanilla MNIST example
    """
    mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

    x = tf.placeholder(tf.float32, [None, 784])

    W = tf.Variable(tf.zeros([784, 10]))
    b = tf.Variable(tf.zeros([10]))

    y = tf.nn.softmax(tf.matmul(x, W) + b)

    y_ = tf.placeholder(tf.float32, [None, 10])

    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    init = tf.initialize_all_variables()

    sess = tf.Session()
    sess.run(init)

    for i in range(1000):
        batch_xs, batch_ys = mnist.train.next_batch(100)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

    correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    print (sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))


def random_learning():
    """ Learn on random data
    """
    N = 4096
    M = 21
    P = 1000

    x = tf.placeholder(tf.float32, [None, N])

    W = tf.Variable(tf.zeros([N, M]))
    b = tf.Variable(tf.zeros([M]))

    y = tf.nn.softmax(tf.matmul(x, W) + b)

    y_ = tf.placeholder(tf.float32, [None, M])

    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

    train_step = tf.train.GradientDescentOptimizer(5).minimize(cross_entropy)

    init = tf.initialize_all_variables()

    sess = tf.Session()
    sess.run(init)

    n_epochs = 10
    batch_xs = np.random.randn(P,N)
    batch_ys = np.zeros((P,M))
    for k in range(P):
        r = int(np.random.randint(0, M))
        batch_ys[k,r]=1
    for i in range(n_epochs):
        print ('epoch %d' %i)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

    correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    print (sess.run(accuracy, feed_dict={x: batch_xs, y_: batch_ys}))


def load_data(filename, categ2key, input_dir, ucats):
    """ Load data from file
    filename : file to read
    categ2key : dictionary mapping taxonomy categories to unique keys
    input_train_dir : training directory, needed to get mean and std values for the CNN distribution
    ucats: array of all unique categories for the entire dataset (not just the file), used for one-hot encoding

    Returns CNN vectors, categories, product IDs and partner IDs
    """
    print ('loading data from %s' % filename)
    # load data
    xx = np.load (filename)
    _cnn = xx[:,:-3]
    _scat = xx[:,-3]
    _productids = xx[:,-2].astype(np.int32)
    _partnerids = xx[:,-1].astype(np.int32)
    
    # map to higher categories
    if categ2key is not None:
        _scat = np.array([categ2key[i] for i in _scat])

    # scale data
    cnn_mean = np.load(os.path.join(input_dir,'a_mean.npy'))
    cnn_std = np.load(os.path.join(input_dir,'a_std.npy'))
    assert(_cnn.shape[1]==cnn_mean.size)
    assert(_cnn.shape[1]==cnn_std.size)
    for k in range(_cnn.shape[1]):
        _cnn[:,k] = (_cnn[:,k] - cnn_mean[k]) / cnn_std[k]
            
    # one-hot encoding of labels
    _cat = onehot (_scat, ucats)

    assert (_cnn.shape[0]==_cat.shape[0])

    return (_cnn, _cat, _productids, _partnerids)


def learn_categories (input_dir_train, input_dir_test, ucats, categ2key, key2name, n_epochs, n_batches, n_batches_valid, failure_filename, depth):
    """ Based on the MNIST example, learn categories from CNN vectors

    ucats: array of all unique categories for the entire dataset, used for one-hot encoding
    categ2key : dictionary mapping taxonomy categories to unique keys
    key2name : dictionary mapping taxonomy keys to category names (for humans)
    n_epochs : # epochs for learning
    n_batches: # files used for learning
    n_batches : # files used for evaluation
    failure_filename : file to log missed classifications
    """
    # init learning
    n_cat = ucats.size

    x = tf.placeholder(tf.float32, [None, 4096])

    W = tf.Variable(tf.zeros([4096, n_cat]))
    #_stddev = 1.0 * 21 / 4096
    #W = tf.Variable(tf.random_normal([4096, n_cat], stddev=_stddev)) # stddev should be #categories / 4096
    
    b = tf.Variable(tf.zeros([n_cat]))

    y = tf.nn.softmax(tf.matmul(x, W) + b)

    y_ = tf.placeholder(tf.float32, [None, n_cat])

    correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    correct_prediction_top5 = tf.nn.in_top_k (y, tf.argmax(y_,1), 5)

    accuracy_top5 = tf.reduce_mean (tf.cast(correct_prediction_top5, tf.float32))

    cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

    learning_rate = 0.01
    train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(cross_entropy)
    #learning_rate = 0.0001
    #train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)

    init = tf.initialize_all_variables()
    sess = tf.Session()
    sess.run(init)
    # log in tensorboard
    now = datetime.now()
    log_dir = "/opt/tflog/.../" + now.strftime("%Y%m%d-%H%M%S") + "-%s/" % depth

    tf.scalar_summary ('accuracy top 1', accuracy)
    tf.scalar_summary ('accuracy top 5', accuracy_top5)
    tf.scalar_summary ('cross-entropy', cross_entropy)
    summary_op = tf.merge_all_summaries()
    #summary_writer = tf.train.SummaryWriter(log_dir, sess.graph)
    train_writer = tf.train.SummaryWriter(log_dir + '/train', sess.graph)
    test_writer = tf.train.SummaryWriter(log_dir + '/test')

    saver = tf.train.Saver()
    save_period = n_epochs

    print ('learning with %d batches.' % n_batches)

    step = 0
    #debug_filename = '/opt/extractFullShuffledTrain/part-00823'
    for epoch in range (n_epochs):
        train_filelist = list(glob.glob(input_dir_train+'/part*.npy'))
        test_filelist = list(glob.glob(input_dir_test+'/part*.npy'))
        random.shuffle (train_filelist)
        random.shuffle (test_filelist)
        train_batch = 0
        if n_batches < 0:
            n_batches = len(train_filelist)

        while train_batch < n_batches and len(train_filelist) > 0:

            filename = train_filelist[0]
            train_filelist = train_filelist[1:]

            (batch_xs, batch_ys, _, _) = load_data (filename, categ2key, input_dir_train, ucats)

            train_batch+=1

            _logger.info ('... training with %d vectors and %d x %d output (depth = %s, epoch = %d, batch = %d, learning rate = %.5f)...' % (batch_xs.shape[0], batch_ys.shape[0], batch_ys.shape[1], depth, epoch, train_batch, learning_rate))

            summary, _, train_accuracy, loss_val = sess.run([summary_op, train_step, accuracy, cross_entropy], feed_dict={x: batch_xs, y_: batch_ys})
            train_writer.add_summary (summary, step)

            step += 1

            _logger.info ('\t[step %3d] [train] [epoch %3d] loss value = %8.5f, train accuracy = %8.5f' % (step, epoch, loss_val, train_accuracy))

            if step % 10 == 0:
                # evaluate
                test_filename = test_filelist[0]
                test_filelist = test_filelist[1:]
                (batch_test_xs, batch_test_ys, productids, partnerids) = load_data (test_filename, categ2key, input_dir_train, ucats)
                summary, test_accuracy, y_val, y__val = sess.run([summary_op, accuracy, y, y_], feed_dict={x: batch_test_xs, y_: batch_test_ys})
                test_writer.add_summary (summary, step)
                _logger.info ('\t[step %3d] [ test] test accuracy = %8.5f' % (step,test_accuracy))
                # log missed classifications in a file
                log_wrong_classifs (y_val, y__val, 5, productids, partnerids, key2name, failure_filename)
            
            if step % save_period == 0:
                checkpoint_path = os.path.join('/opt/models/', 'model-%s.ckpt' % depth)
                saver.save(sess, checkpoint_path, global_step=step)
                _logger.info ('Saved model to %s' % checkpoint_path)


def log_wrong_classifs (y, y_, topk, productids, partnerids, key2name, out_filename):
    """
    Log wrong classifications (beyond topk) in a file

    y : predicted labels, as an M x P matrix of floats (M exemples, P one-hot values)
    y_ : true labels, as an M x P matrix of binary values where 1 = true label
    productids : vector of product ids, size M
    partnerids : vector of partner ids, size M
    key2name : directory mapping labels to category names (for humans)
    """
    for k in range(y.shape[0]):
        z = util.argtopn (y[k,:],topk)
        intopk = False
        for zz in z:
            if y_[k,zz]==1:
                intopk = True
                break
        if not intopk:
            productid = productids[k]
            partnerid = partnerids[k]
            scores = [y[k,i] for i in z]
            truecateg = key2name[util.argtopn(y_[k],1)[0]]
            predcateg = [key2name[i] for i in z]
            with open (out_filename,'a') as kp:
                kp.write ('%d,%d,%s,%s\n' % (partnerid, productid, truecateg, ','.join(['%s,%.5f'%(u,v) for (u,v) in zip(predcateg,scores)])))
        


def dataset_analysis (cnn, cat):
    """ Analyse distance distribution between CNN vectors in the dataset
    """
    global _logger

    random.seed (time.time())

    avg_d = .0
    n_d = 0

    # pick a random category
    #categ = cat[random.randint(0,cat.size-1)]
    for categ in np.unique(cat):
        # find all corresonding vectors
        ind = np.nonzero(cat==categ)[0]
        x = cnn[ind,:]

        nel=x.shape[0]

        # compute pair-wise distances for 1000 vectors
        np.random.shuffle(ind)
        if nel>1000:
            ind = ind[:1000]
        nel=ind.size
        if nel < 2:
            continue
        neq=0
        ntest=0
        for k1 in range(nel-1):
            for k2 in range(k1+1,nel):
                x1 = x[k1,:]
                x2 = x[k2,:]
                d = np.dot(x1,x2)
                ntest+=1
                if d > .9:
                    neq += 1

        dist = 100.0*neq/ntest
        avg_d += dist
        n_d += 1
        _logger.info ('categ %10s: %5d / %5d similar products (%6.2f%%)' % (categ, neq, ntest, dist))

    _logger.info('Average rate of similar products: %6.2f' % (1.0 * avg_d/n_d))


def onehot (vect, ref):
    """ Return a onehot representation of the input vector (assumed to be ints)
    """
    out = np.zeros((len(vect),ref.size),dtype=np.int8)
    count=0
    for v in vect:
        assert(v in ref)
        if not v in ref:
            print (ref)
            print (v)
        i = util.index(ref,v)
        out[count,i]=1
        count+=1
    return out


def set_logger ():
    global _logger

    # init logger
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(format=FORMAT)
    _logger = logging.getLogger('learning')
    _logger.setLevel(logging.INFO)
    fh = logging.FileHandler ('log/learning.log')
    fh.setLevel (logging.INFO)
    formatter = logging.Formatter (FORMAT)
    fh.setFormatter (formatter)
    _logger.addHandler(fh)


def process_dirs (input_train_dir, input_test_dir, categ2key, key2name, n_epochs, n_batches, n_batches_valid, failure_filename, depth):

    global _logger

    # stats categories
    categories_dist_file = os.path.join (input_train_dir,'categories-distribution-%s.csv'%depth)
    if os.path.isfile (categories_dist_file):
        categ_dist = util.read_dict (categories_dist_file)
    else:
        _logger.info('computing categories distribution from %s...' % input_train_dir)
        categories = util.list_categories (input_train_dir, categ2key)
        categ_dist = util.dist_stats (categories)
        util.dict_to_file (categ_dist, categories_dist_file)

    ucats = np.array([k for k,_ in categ_dist.iteritems()])

    # train model
    learn_categories(input_train_dir, input_test_dir, ucats, categ2key, key2name, n_epochs, n_batches, n_batches_valid, failure_filename, depth)


def main():
    global _logger

    set_logger()

    #random_learning()
    #return

    #mnist_example ()
    #return

    # read config file
    config = ConfigParser.ConfigParser()
    config.readfp(open('defaults.ini'))
 
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', dest='n_epochs', type=int, help='number of epochs in learning', required=True)
    parser.add_argument('--depth', dest='depth', type=str, help='depth of the taxonomy', required=True)
    parser.add_argument('--batches', dest='batches', type=int, help='number of batches for learning (-1 for max)', required=False, default=-1)
    parser.add_argument('--valid', dest='batches_valid', type=int, help='number of batches for validation', required=False, default=10)
    parser.add_argument('--logdatafile', dest='logdatafile', type=str, help='output file for variable tracking', required=True)
    args = parser.parse_args()

    # reset datafile
    with open (args.logdatafile,"w"):
        pass

    # load google taxonomy
    tax = util.load_taxonomy ()

    # compute category mapping
    depth = args.depth
    _logger.info("Building taxonomy with depth = %s" % depth)
    (categ2key, _, key2name, _) = util.taxonomy_map_at_depth (tax, depth)

    input_dir_train = '/opt/extractFullShuffledTrainSampled'
    input_dir_test = '/opt/extractFullShuffledTestSampled'

    n_epochs = args.n_epochs
    logdatafile = args.logdatafile
    n_batches = args.batches
    n_batches_valid = args.batches_valid

    # reset failure file
    failure_filename = '/opt/fails-%d-%s.csv' % (n_epochs, depth)
    
    with open (failure_filename,'w'):
        pass

    #with tf.device ("/job:localhost/replica:0/task:0/gpu:0"):
    process_dirs (input_dir_train, input_dir_test, categ2key, key2name, n_epochs, n_batches, n_batches_valid, failure_filename, depth)


if __name__ == "__main__":

    main()





