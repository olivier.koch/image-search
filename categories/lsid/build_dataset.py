import argparse
import glob
import os
import random
import re
import sys
sys.path.append('../')
import util
import subprocess
import time

def main():
    """ split an image dataset into training and testing set, as expected in https://github.com/tensorflow/models/tree/master/inception
    """

    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory containing images', required=True)
    parser.add_argument('--train_dir', dest='train_dir', type=str, help='output training directory', required=True)
    parser.add_argument('--test_dir', dest='test_dir', type=str, help='output test directory', required=True)
    parser.add_argument('--ratio', dest='ratio', type=float, help='train/test ratio in the 0-1 range', required=False, default=0.8)
    args = parser.parse_args()

    # list images
    files = glob.glob (os.path.join (args.input_dir, '*/*.jp*g'))

    random.shuffle(files)

    n_files = len(files)
    n_split = int(args.ratio * n_files)

    train_files = files[:n_split]
    test_files = files[n_split:]

    # reset output dirs
    util.reset_dir (args.train_dir)
    util.reset_dir (args.test_dir)

    # filename format ends in <categoryid>/<source>_<imageid>.jpg (ex /ops/lsid/images/3245/google_231.jpg)
    for filename in train_files:
        partial_name = '/'.join(filename.split('/')[-2:])
        link_name = os.path.join (args.train_dir, partial_name)
        link_dirname = os.path.dirname (link_name)
        if not os.path.isdir (link_dirname):
            os.makedirs (link_dirname)
        os.symlink (filename, link_name)
        print ('%100s --> %50s' % (filename, link_name))
    for filename in test_files:
        partial_name = '/'.join(filename.split('/')[-2:])
        link_name = os.path.join (args.test_dir, partial_name)
        link_dirname = os.path.dirname (link_name)
        if not os.path.isdir (link_dirname):
            os.makedirs (link_dirname)
        os.symlink (filename, link_name)
        print ('%100s --> %50s' % (filename, link_name))

    print ('%d images, %d in training set, %d in testing set' % (n_files, n_split, n_files-n_split))


if __name__ == "__main__":
    main()

