import argparse
import util
import glob
import numpy as np
import subprocess
import os
import multiprocessing
import random
import shutil
import re

def remove_invalid_jpegs (files, proc_id):
    """ remove images that are not valid jpegs
    """
    err_count=0
    fix_count=0
    total_count=0
    progress=0
    n_files = len(files)
    for k,filename in enumerate(files):
        # file may not exist due to previous cleanup
        if not os.path.isfile (filename):
            continue
        total_count+=1
        if not util.check_jpeg_file (filename):
            os.remove (filename)
            fix_count+=1
        if 100.0*k/n_files>progress:
            progress+=1
            if progress%10==0:
                print ('[%d] progress = %d %%'% (proc_id, progress))

    print ('[jpeg validation] %d total, %d removed' % (total_count, fix_count))


def remove_non_white_background_images (files, proc_id):
    """ remove images that do not have a white background
    """
    err_count=0
    fix_count=0
    total_count=0
    progress=0
    n_files = len(files)

    for k,filename in enumerate(files):
        # file may not exist due to previous cleanup
        if not os.path.isfile (filename):
            continue
        total_count+=1
        tmp_filename = '/tmp/.cleanup_%d.jpg' % proc_id
        (_,rw) = util.count_white_pixels (filename,tmp_filename)
        if rw < 0.1:
            #shutil.copy (filename, '/tmp/non-white')
            #os.remove (filename)
            fix_count+=1
            #shutil.copy (filename, '/tmp/white')
        if 100.0*k/n_files>progress:
            progress+=1
            if progress%10==0:
                print ('[%d] progress = %d %%'% (proc_id, progress))

    print ('[white background] %d total, %d removed' % (total_count, fix_count))


def remove_invalid_filenames (files, proc_id):
    """ Remove images that do not match the following filename formats: yandex_%d.jpg, yahoo_%d.jpg, google_%d.jpg
    """
    patterns = [re.compile(a) for a in ['yandex_\d+.jpg', 'yahoo_\d+.jpg','google_\d+.jpg']]
    fix_count=0
    total_count=0
    progress=0
    n_files = len(files)
    for k,filename in enumerate(files):
        # file may not exist due to previous cleanup
        if not os.path.isfile (filename):
            continue
        total_count+=1
        valid=False
        for patt in patterns:
            if patt.search(filename) is not None:
                valid=True
                break
        if not valid:
            os.remove (filename)
            fix_count+=1
        if 100.0*k/n_files>progress:
            progress+=1
            if progress%10==0:
                print ('[%d] progress = %d %%'% (proc_id, progress))

    print ('[invalid filenames] %d total, %d removed' % (total_count, fix_count))



def parallel_run (files, proc_id):
    """ check invalid images
    """
    print ('processing %d images.' % len(files))
   
    #remove_non_white_background_images (files, proc_id)

    remove_invalid_jpegs (files, proc_id)

    remove_invalid_filenames (files, proc_id)


def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--indir', dest='input_dir', type=str, help='input directory', required=True)
    parser.add_argument('--num_procs', dest='n_procs', type=int, help='number of processes', required=True)
    args = parser.parse_args()

    # list images
    image_list = []
    dirs = util.list_dirs (args.input_dir)
    for dirname in dirs:
        files = glob.glob(dirname+'/*.jp*g')
        image_list += files

    image_list += glob.glob(os.path.join (args.input_dir,'*.jp*g'))

    print ('%d images to process.' % len(image_list))

    # split
    random.shuffle(image_list)
    image_lists = [[] for k in range(args.n_procs)]
    for k,f in enumerate(image_list):
        image_lists[k%args.n_procs].append(f)
    
    util.reset_dir ('/tmp/white')
    util.reset_dir ('/tmp/non-white')

    # launch jobs
    jobs = []
    for proc in range(args.n_procs):
        process = multiprocessing.Process (target=parallel_run, args=[image_lists[proc],proc])
        process.start()
        jobs.append(process)

    # wait for all
    for job in jobs:
        job.join()



if __name__ == "__main__":
    main()

