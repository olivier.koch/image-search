#!/usr/bin/env python
# Baloo : l'ours, il enseigne la Loi de la Jungle et les langues des Peuples de la Jungle aux jeunes louveteaux et a Mowgli, parfois trop durement.
#
# This script downloads images from HDFS and ships them to AWS for processing
#

import subprocess
import os
import sys
import json
import datetime
import time
from util import util_send_email

def _log (msg):
    dt_string = datetime.datetime.now().strftime ('%Y-%m-%d %I:%M%p')
    with open('/home/o.koch/image-search/_%s.log' % os.path.basename(__file__) ,'a') as fp:
        print ('%s\t%s' % (dt_string, msg))
        fp.write ('%s\t%s\n' % (dt_string, msg))
        fp.close()

def main():

    dry_run = True

    # header for the log file
    _log ('======================================================================')
    _log ('======================================================================')
    _log ('======================================================================')

    # get timestamp
    dt_string = '%d' % int(time.time())

    json_filename = '/home/o.koch/image-search/outputByPartner.json'
    timestamp_filename = '/home/o.koch/image-search/lastPartnerTimestamp.txt'
    root_dir = '/user/recocomputer/bestofs/imagedl/'

    # parse JSON data
    with open(json_filename,'r') as fp:
        json_data = fp.read()
        try:
            data = json.loads(json_data)
        except:
            _log('Failed to read JSON file %s.  File might be empty.  Exiting.' % json_filename)
            return

    # load last timestamp for each partner
    last_partnersTimestamp = {}
    if os.path.isfile (timestamp_filename):
        with open(timestamp_filename,'r') as fp:
            lines = fp.readlines()
            for l in lines:
                l = l.strip().split(' ')
                partnerid = int(l[0])
                timestamp = int(l[1])
                last_partnersTimestamp[partnerid] = timestamp

    n_transferred_files = 0
    n_transferred_partners = 0

    # parse json file
    for item in data:
        partner_id = int(item)
        partner_timestamp = int(data[item]['jobTimeStamp'])

        if  partner_id in last_partnersTimestamp and last_partnersTimestamp[partner_id] >= partner_timestamp:
            _log('Skipping partner %d.  No new data to process. Current timestamp : %d.  Last timestamp :%d.  Current - last = %d.' % (partner_id, \
                    partner_timestamp, last_partnersTimestamp[partner_id], partner_timestamp - last_partnersTimestamp[partner_id]))
            continue

        last_partnersTimestamp[partner_id] = partner_timestamp
        n_transferred_partners += 1
        _log('Processing partner %d with timestamp %d' % (partner_id, partner_timestamp))

        # get file
        output_folder = data[item]['outputFolder']
        files = data[item]['files']
        for file in files:
            target = os.path.join(root_dir,output_folder,file)
            print (target)
            # copy from HDFS
#            cmd = 'hadoop fs -get %s %s' % (target, local_file)
#            _log(cmd)
#            if not dry_run:
#                with open(os.devnull,'w') as fnull:
#                    subprocess.call (cmd, shell=True, stdout=fnull, stderr=fnull)




if __name__ == "__main__":

    # header for the log file
    _log ('======================================================================')
    _log ('======================================================================')
    _log ('======================================================================')

    main()

