import cv2
import os, sys
import configparser
import numpy as np
from util import *
import multiprocessing
import pickle
import copy
from fisher import fisher_compute_gmm, fisher_compute_vectors
import random
import math
import glob

def compute_HOG (im, hog_resize):
    winSize = (64,64)
    blockSize = (16,16)
    blockStride = (16,16)
    cellSize = (8,8)
    nbins = 9
    derivAperture = 1
    winSigma = 4.
    histogramNormType = 0
    L2HysThreshold = 2.0000000000000001e-01
    gammaCorrection = 0
    nlevels = 64
    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,\
                    histogramNormType,L2HysThreshold,gammaCorrection,nlevels)
    winStride = (64,64)
    padding = (0,0)
    locations = ()
    h = hog.compute(im, winStride, padding, locations)

    # resample
    lenh = len(h)
    if hog_resize < lenh:
        K = np.zeros((hog_resize,1))
        r = math.floor(1.0*lenh/hog_resize)
        for k in range(hog_resize):
            K[k] = np.mean(h[k*r:(k+1)*r])
        h = K

    # normalize
    hl = np.linalg.norm(h)
    if hl > 1E-8:
        h = h / hl

    #print(len(h))

    return h

def compute_RGB_histogram (im):
    hr = cv2.calcHist ([im],[0],None,[64],[0,256])
    hg = cv2.calcHist ([im],[1],None,[64],[0,256])
    hb = cv2.calcHist ([im],[2],None,[64],[0,256])
    v = np.hstack((hr,hg,hb)).flatten()
    hv = np.linalg.norm(v)
    if hv > 1E-8:
        v = v / hv
    return v

def compute_surf_features (im):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    hessianThreshold = 800 # twice the default value in opencv
    descriptors = None
    while descriptors is None and hessianThreshold > 0.001:
        hessianThreshold /= 1.414
        surf = cv2.xfeatures2d.SURF_create(hessianThreshold=hessianThreshold, extended=False, upright=True)
        _, descriptors = surf.detectAndCompute (gray, None)
    if descriptors is None:
        return np.zeros((1,64))
    return descriptors

def list_images (listing_file):
    fp = open (listing_file, 'r')
    lines = fp.readlines()
    fp.close()
    n_images = len(lines)
    print('%d images to process.' % n_images)
    return [x.strip() for x in lines]


def compute_features (proc_num, input_file, feature_type, im_r_size, hog_resize, gmm, gmm_n_words, feature_filename):

    image_list = list_images(input_file)
    n_images = len(image_list)
    print('%d images to process (resized to %d x %d)' % (n_images, im_r_size, im_r_size))

    out_vec = None
    feature_size_vec = None

    if feature_type in ['hog', 'surf']:
        feature_size = 0
        rgb_hist_size = 0
        for (img_name,count) in zip (image_list,range(1,n_images+1)):

            # read image
            im = cv2.imread(img_name)

            # resize
            im = cv2.resize(im, (im_r_size,im_r_size))

            if feature_type == 'hog':
                fv = compute_HOG(im, hog_resize)
                feature_size = fv.size
            elif feature_type == 'surf':
                fv = compute_surf_features(im)
                feature_size = fv.shape[1]

            if count==1:
                print ('Feature length : %d' % feature_size)

            # compute RGB histogram
            rgb_hist = compute_RGB_histogram (im)
            rgb_hist_size = rgb_hist.size

            # store to memory
            if feature_type == 'hog':
                if out_vec is None:
                    out_vec = np.zeros((n_images, feature_size + rgb_hist_size))
                out_vec[count-1,:] = np.append(fv, rgb_hist)
            if feature_type == 'surf':
                if out_vec is None:
                    out_vec = fv
                else:
                    out_vec = np.vstack ((out_vec,fv))

            # print progress bar
            if count % int(n_images/10) == 0:
                print ('[process %d] Progress %d%%' % (proc_num,100.0*count/n_images))

            feature_size_vec = [feature_size, rgb_hist_size]

    elif feature_type == "fisher":

        out_vec = fisher_compute_vectors(gmm, image_list, gmm_n_words, im_r_size)

        feature_size_vec = [out_vec.shape[1],0]

    else:
        print ('*** ERROR *** Unsupported feature type %s' % feature_type)
        return

    # save features to file
    # we save them to separate files in order to parallelize feature matching later on

    # may not need to deep copy...
    # pickle.dump((image_list,copy.deepcopy((out_vec,feature_size_vec))), open(feature_filename, 'wb'))
    pickle.dump((image_list,(out_vec,feature_size_vec)), open(feature_filename, 'wb'))
    print ('Saved set %d (%d images, %d x %d) in file %s' % (proc_num, n_images, out_vec.shape[0], out_vec.shape[1], feature_filename))


if __name__ == "__main__":

    # read config file
    config = configparser.ConfigParser()
    config.readfp(open('defaults.ini'))

    # read list of images
    input_file = config.get ('Catalog','input_file')
    partnerid = util_get_partnerid_from_file(input_file)
    data_dir = config.get ('Catalog','data_dir')
    max_images = config.getint('Features','max_images')
    im_r_size = config.getint('Features','resize')
    input_file = os.path.join (data_dir, 'imlist-%d.txt' % partnerid)

    n_chunks = config.getint ('Features','n_chunks')
    n_chunks_x = config.getint('Matching','n_chunks_x')
    n_chunks_y = config.getint('Matching','n_chunks_y')
    feature_type = config.get ('Features','feature_type')
    gmm_n_words = config.getint('Features','gmm_n_words')
    pca_feature_size = config.getint('Features','pca_feature_size')
    hog_resize_list = [int(x) for x in config.get('Features','hog_resize').split(' ')]
    feature_file_basename = config.get('Features','feature_file_basename')

    nlines = util_count_lines (input_file)
    sampling_ratio = 1.0 * max_images / nlines

    if n_chunks % 16 != 0:
        print ('*** ERROR ***  Number of chunks must be a multiple of 16 ***')
        sys.exit(1)


    n_batches = int(n_chunks / 16)

    if n_chunks_x == 0:
        n_chunks_x = n_chunks
    if n_chunks_y == 0:
        n_chunks_y = n_chunks

    # split input file into N files
    print ('Splitting input file %s into %d files (sampling ratio = %.3f)...' % (input_file, n_chunks,sampling_ratio))
    temp_files = util_split_file (input_file, n_chunks, sampling_ratio)
    print ('done.')

    # compute GMM for Fisher vectors
    gmm = None
    if feature_type == "fisher":
        features_file_list = glob.iglob('data/features/features-%d-surf-*.bin' % partnerid)
        gmm = fisher_compute_gmm (features_file_list, gmm_n_words, pca_feature_size, im_r_size)

    if feature_type != 'hog':
        hog_resize_list = [1] # dummy value, we won't loop over hog sizes

    for hog_resize in hog_resize_list:
        print ('*** Processing hog size %d ***' % hog_resize)

        jobs = []
        #manager = multiprocessing.Manager()
        #return_dict = manager.dict()

        for b in range(n_batches):
            print ('*** Batch %d / %d ***' % (b+1, n_batches))
            for k in range(16):
                n = b*16+k
                # compute_features
                feature_filename = util_feature_filename (data_dir, feature_file_basename, partnerid, feature_type, hog_resize, n)
                process = multiprocessing.Process (target=compute_features, args=[n,temp_files[n],feature_type,im_r_size,hog_resize,gmm,gmm_n_words,feature_filename])
                process.start()
                jobs.append (process)

            # wait for everyone
            for proc in jobs:
                proc.join()

    # remove temp files
    for temp_file in temp_files:
        os.remove(temp_file)

