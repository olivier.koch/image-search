#!/usr/bin/env python
import os
import re

def main():
    
    coevents_dir = '/opt/coevents'

    output_csv = '/opt/coevents/coevents.csv'
    
    fo = open (output_csv, 'a')

    g_n_found = 0
    g_n_trials = 0

    for cv_file in os.listdir (coevents_dir):


        # extract partner id from filename
        d = re.match ('coevents-([0-9]+).csv', cv_file)
        if d is None:
            continue
        
        print ('processing %s...' % cv_file)
        
        partnerid = int(d.group(1))

        # read pairs
        n_trials = 0
        n_found = 0

        already_seen = {}


        with open(os.path.join(coevents_dir,cv_file),'r') as fh:
            lines = fh.readlines()
            for line in lines:
                ids = [int(x) for x in line.split(',')]
                key_id = ids[0]
                val_id = ids[1]

                keys = (key_id, val_id)
                keys_rev = (val_id, key_id)
                if keys in already_seen or keys_rev in already_seen:
                    continue
            
                already_seen[keys] = 1

                # build image names
                img_key = '%d__%d.jpg' % (partnerid, key_id)
                img_val = '%d__%d.jpg' % (partnerid, val_id)

                img_dirname = '/opt/_img'

                full_img_name_key = os.path.join (img_dirname, img_key)
                full_img_name_val = os.path.join (img_dirname, img_val)

                #print (full_img_name_key)
                #print (full_img_name_val)

                if os.path.isfile (full_img_name_key) and os.path.isfile (full_img_name_val):
                    n_found += 1
                    g_n_gound += 1
                    fo.write ('%d,%d,%d\n' % (partnerid, key_id, val_id))

                n_trials += 1
                g_n_trials += 1
        
        print ('%d/%d images found' % (n_found, n_trials))

    print ('Success rate %.2f %%.  %d pairs found.' % ( 100.0 * g_n_found / g_n_trials, g_n_found))
    fo.close()
        
if __name__ == "__main__":
    main()
